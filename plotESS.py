import pandas as pd
import os
import numpy as np
import matplotlib.pyplot as plt
import sys

# takes as input the directory where esses_..csv files are.
indir = sys.argv[-1]

all_df = pd.DataFrame()

for f in os.listdir(indir):
    if f.endswith("csv") and f.startswith("esses"):
        df = pd.read_csv(os.path.join(indir, f))
        df_m = pd.melt(df.reset_index(), id_vars="index")
        df_m["run_id"] = f
        df_m["sim_id"] = df_m.run_id.str[10:16]
        all_df = pd.concat([all_df, df_m])
all_df["value"] = all_df.value.astype(float)

all_df_med = pd.pivot_table(data=all_df, index="index", columns="variable", values="value", aggfunc=np.median)

plt.matshow(all_df_med, aspect="auto", vmin=200) # remove or change vmax to change the threshold wbove which the color is saturated.
plt.colorbar()
plt.tight_layout()
plt.show()
