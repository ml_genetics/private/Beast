#! /bin/bash
#SBATCH -J Beast 
#SBATCH --output=/home/jcury/work/Beast/log/beast_%A.out
#SBATCH --error=/home/jcury/work/Beast/log/beast_%A.error
#SBATCH --ntasks=1 
#SBATCH --mem-per-cpu=4G
#SBATCH --array=1-2 #1295 # 1295 elt in validation set


echo "=====Job Informations==== "
echo "Node List: " $SLURM_NODELIST
echo "jobID: " $SLURM_JOB_ID
echo "Partition: " $SLURM_JOB_PARTITION
echo "submit directory:" $SLURM_SUBMIT_DIR
echo "submit host:" $SLURM_SUBMIT_HOST
echo "In the directory: `pwd`"
echo "As the user: `whoami`"

eval "$(conda shell.bash hook)"
conda activate /work/jcury/Beast/beast_env 
cd /work/jcury/Beast/Beast

repID=$(awk 'NR=='$SLURM_ARRAY_TASK_ID'' /home/jcury/work/Beast/Beast_on_Validation/list_rep000_Validationset)

echo "$(date) STARTING $repID" > /home/jcury/work/Beast/log/"$SLURM_ARRAY_TASK_ID"_"$repID".log 2>&1
echo "$(date) Running XML" >> /home/jcury/work/Beast/log/"$SLURM_ARRAY_TASK_ID"_"$repID".log 2>&1
./run_xml.sh /home/jcury/work/Beast/Validation_dataset/rep_000_all_ValidationSet/"$repID".npz /home/jcury/work/Beast/Beast_on_Validation/beast_log /home/jcury/work/Beast/Beast_on_Validation/config >> /home/jcury/work/Beast/log/"$SLURM_ARRAY_TASK_ID"_"$repID".log 2>&1
echo "$(date) Running BEAST" >> /home/jcury/work/Beast/log/"$SLURM_ARRAY_TASK_ID"_"$repID".log 2>&1
./run_beast.sh /home/jcury/work/Beast/Beast_on_Validation/beast_log/"$repID".xml >> /home/jcury/work/Beast/log/"$SLURM_ARRAY_TASK_ID"_"$repID".log 2>&1
echo "$(date) DONE $repID" >> /home/jcury/work/Beast/log/"$SLURM_ARRAY_TASK_ID"_"$repID".log 2>&1
