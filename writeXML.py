#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os, sys, subprocess, pandas as pd
import snp2xml as sx

def usage():
    print("""
    Convert the input ms simulation to XML file

    input :
        -in : the simulation
        -out : the output directory

    usage :
    ./writeXML.py -in <path/to/input>
    """)

if("-h" in sys.argv):
	usage()
	sys.exit()

try:
    input = sys.argv[sys.argv.index("-in") +1]
    outdir = sys.argv[sys.argv.index("-out") +1]
    conf = sys.argv[sys.argv.index("-xml") +1]

except:
    usage()
    sys.exit()


"""
Use the good function depending on the file extension and add the beast
option at the end of the outut files.
"""

dico = {'out':os.path.splitext(input)[0]}
dico["out"] = dico["out"].replace(os.path.dirname(dico["out"]), outdir)

with open(conf,"r") as conf:
    for line in conf:
        if not line.startswith('#'):
            key, value = line.split(" : ")
            value = value.replace("\n","")
            if key == "length":
                dico[key] = float(value)
            else:
                dico[key] = value
#dico = pd.DataFrame.from_csv(csv, sep="\t", header=None).to_dict()[1]

ext = os.path.splitext(input)[1]

header = sx.template_header()

if ext == ".fasta":
    seq = sx.fst_converter(input)

elif ext == ".msout" or ext == ".npz":
    seq = sx.ms_converter(input, **dico)

else:
    print("Wrong file")

footer = sx.footer(**dico)


#Open a XML output.
with open(dico["out"]+".xml", "w") as outfile:
    outfile.write(header+seq+footer)
