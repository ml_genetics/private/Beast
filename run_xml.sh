#!/bin/bash


# Takes as input as npz and a config file, returns a xml to give to beast

WDIR_PATH=$(dirname "$0")

npzfile=$1
outdir=$2
config=$3

mkdir -p "$outdir"

"$WDIR_PATH"/writeXML.py -in "$npzfile" -out "$outdir" -xml "$config"