#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys, pandas as pd, os.path

def usage():
    print("""
    Create a dictionary with the parameters for the Beast analysis

    input :
        -out : the file name for the output file - csv
        -ind : number of individuals
        -len : sequence length - default 20000

    usage :
    ./dico.py -out $name
    """)

if("-h" in sys.argv):
	usage()
	sys.exit()

try:
    out = sys.argv[sys.argv.index("-out") +1]
except:
    usage()
    sys.exit()

try:
    ind = sys.argv[sys.argv.index("-ind") +1]
except:
    ind = 50

try:
    len = sys.argv[sys.argv.index("-len") +1]
except:
    len = 20000

try:
    MCMC = sys.argv[sys.argv.index("-mcmc") +1]
except:
    MCMC = 3000000

try:
    logEvery = sys.argv[sys.argv.index("-logE") +1]
except:
    logEvery = 3000

try:
    logEBSP = sys.argv[sys.argv.index("-logEB") +1]
except:
    logEBSP = 1000

try:
    initPop = sys.argv[sys.argv.index("-initpop") +1]
except:
    initPop = 3.2e-4

try:
    maxPop = sys.argv[sys.argv.index("-maxpop") +1]
except:
    maxPop = 100

try:
    minPop = sys.argv[sys.argv.index("-minpop") +1]
except:
    minPop = 0

try:
    moyLogNorm = sys.argv[sys.argv.index("-mlogn") +1]
except:
    moyLogNorm = 0

try:
    varLogNorm = sys.argv[sys.argv.index("-vlogn") +1]
except:
    varLogNorm = 0.25

try:
    lambdaPois = sys.argv[sys.argv.index("-lambda") +1]
except:
    lambdaPois = 0.7

out=os.path.splitext(out)[0]

dico = {'ind':ind, 'length':len, 'out':out, 'loci':'1', 'MCMC':MCMC, 'logEvery':logEvery, \
'logEBSP':logEBSP, 'initPop':initPop, 'maxPop':maxPop, 'minPop':minPop, 'moyLogNorm':moyLogNorm, \
'varLogNorm':vlogn, 'lambdaPois':lambdaPois}

#Save the parameters for the simulation
pd.DataFrame.from_dict(dico,orient="index").to_csv("{}.csv".format(out), sep="\t", header=False)
