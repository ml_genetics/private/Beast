#!/usr/bin/env python
# -*- coding: utf-8 -*-
import utils
import os, fnmatch, subprocess
import numpy as np
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord
from Bio import SeqIO

def searchSNP(P, simu, seq):
    """
    Create a dictionary containing the SNP positions.
    """
    #Complement and SNP dictionaries
    comp = {'A':['T','C','G'], 'T':['A','C','G'], 'G':['A','C','T'], 'C':['A','T','G']}
    snp = {'A': {'x':[], 'y':[]},
           'T': {'x':[], 'y':[]},
           'C': {'x':[], 'y':[]},
           'G': {'x':[], 'y':[]}}

    for i,ss in enumerate(simu.T):  # For each snp
        indiv_with_snp = np.where(ss)[0]  #Position where ss == 1
        mutation = np.random.choice(comp[seq[P[i]]])  # Random mutation in site P[i]
        
        if P[i] not in snp[mutation]['y']:
            snp[mutation]['x'] += indiv_with_snp.tolist()
            snp[mutation]['y'] += [P[i]]*len(indiv_with_snp)

    return snp

def template_header():
    """
    Build the head of the XML file.
    """
    return """<beast version='2.0'
    namespace='beast.evolution:beast.evolution.alignment:beast.core:beast.evolution.tree.coalescent:beast.core.util:beast.evolution.nuc:beast.evolution.operators:beast.evolution.sitemodel:beast.evolution.substitutionmodel:beast.evolution.likelihood'>
    """


def fst_converter(fstfile):
    """
    Convert a fasta file into a XML file.
    """
    path = "/".join(fstfile.split("/")[0:-1])
    os.chdir(path)
    middle="\t\t<data id=\"alignment\" dataType=\"nucleotide\">\n"

    with open(fstfile,"r") as input:

        for line in input.readlines():
            if line[0]=='>':
                middle+="\t\t\t\t<sequence taxon=\"{}\">\n".format(line[1:-1])
            else:
                middle+="\t\t\t\t\t{}\n".format(line)
                middle+="\t\t\t\t</sequence>\n"

        middle+="\t\t</data>\n"

    return middle

def ms_converter(msfile, **dico):
    """
    Convert the simulation into XML file.
    """
    filename, ext = os.path.splitext(msfile)
    middle=""

    #One file per repetition
    #subprocess.call("split -p \"//\" "+msfile, shell=True)

    #Count alignments
    nb_al = 0


    #If npzfile doesnt exists, create it
    if not(os.path.isfile(filename+".npz")):
        utils.convert_ms(msfile)

    simu, pos = utils.read_ms_compressed(filename+".npz")
    pos = (pos*int(dico['length'])).astype(int) #SNP positions
    
    if int(dico["length"]) in pos:
        pos[np.where(pos == dico["length"])] = dico["length"] - 1

    if "trunc_indiv" in dico.keys():
        idx = np.random.choice(simu.shape[0], int(dico["trunc_indiv"]), replace=False)
        sub_snp = simu[idx,:]
        snp_2_keep = (sub_snp[0, :] == sub_snp).all(axis=0) == False
        sub_snp = sub_snp[:, snp_2_keep]
        sub_pos = pos[snp_2_keep]
    else:
        sub_snp = simu
        sub_pos = pos

    if "trunc_snp" in dico.keys():
        sub_snp = sub_snp[:, :int(dico["trunc_snp"])]
        sub_pos = sub_pos[:int(dico["trunc_snp"])]
        dico["length"] = sub_pos[-1] + 1 # sequence is truncated

    seq_anc = np.random.choice(["A","T","C","G"], int(dico['length']))
    aln_anc = np.array([seq_anc.tolist()]*len(sub_snp))
    SNP = searchSNP(sub_pos, sub_snp, seq_anc)

    for nuc in SNP.keys():
        aln_anc[SNP[nuc]['x'], SNP[nuc]['y']] = nuc
    
    #print(f"N snp in aln : {aln_anc.shape[1] - (aln_anc[0] == aln_anc).all(axis=0).sum()}")

    #Alignment
    seqfst = [SeqRecord(Seq("".join(i)), id="Seq_{:03}".format(j+1)) for j, i in enumerate(aln_anc)]


    # if dico['loci'] == '1':
    middle+="\t\t<data id=\"alignment\" dataType=\"nucleotide\">\n"

    # else:
    #     middle+="\t\t<data id=\"alignment{:03}\" dataType=\"nucleotide\">\n".format(nb_al)

    #Loop foreach ms file
    nb_al += 1

    for seq in seqfst:
        middle+="""\t\t\t\t<sequence taxon=\"{}\">
        \t\t{}
        </sequence>
        """.format(seq.id,seq.seq)

    middle+="""\t\t</data>
    """

    return middle


def footer(**dico):
    """
    Build the footer of the XML file when using BSP analysis.
    """
    if dico["type"]=="EBSP":
        return """
        <map name="Uniform" >beast.math.distributions.Uniform</map>

        <map name="Exponential" >beast.math.distributions.Exponential</map>

        <map name="LogNormal" >beast.math.distributions.LogNormalDistributionModel</map>

        <map name="Normal" >beast.math.distributions.Normal</map>

        <map name="Beta" >beast.math.distributions.Beta</map>

        <map name="Gamma" >beast.math.distributions.Gamma</map>

        <map name="LaplaceDistribution" >beast.math.distributions.LaplaceDistribution</map>

        <map name="prior" >beast.math.distributions.Prior</map>

        <map name="InverseGamma" >beast.math.distributions.InverseGamma</map>

        <map name="OneOnX" >beast.math.distributions.OneOnX</map>

        <run id="mcmc" spec="MCMC" chainLength="{MCMC}" storeEvery="3000">

            <state id="state" storeEvery="5000">

                <tree id="Tree.t:alignment" name="stateNode">

                    <taxonset id="TaxonSet.alignment" spec="TaxonSet">

                        <alignment idref="alignment"/>

                    </taxonset>

                </tree>

                <parameter id="kappa.s:alignment" lower="0.0" name="stateNode">{kappa}</parameter>

                <stateNode id="indicators.alltrees" spec="parameter.BooleanParameter">false</stateNode>

                <parameter id="populationMean.alltrees" name="stateNode">1.0</parameter>

                <parameter id="popSizes.alltrees" lower="{minPop}" name="stateNode" upper="{maxPop}">{initPop}</parameter>

            </state>

            <init id="RandomTree.t:alignment" spec="beast.evolution.tree.RandomTree" estimate="false" initial="@Tree.t:alignment" taxa="@alignment">

                <populationModel id="ConstantPopulation0.t:alignment" spec="ConstantPopulation">

                    <parameter id="randomPopSize.t:alignment" name="popSize">1.0</parameter>

                </populationModel>

            </init>

            <distribution id="posterior" spec="util.CompoundDistribution">

                <distribution id="prior" spec="util.CompoundDistribution">

                    <distribution id="ExtendedBayesianSkyline.t:alignment" spec="Coalescent">

                        <populationModel id="scaledDemo.t:alignment" spec="ScaledPopulationFunction">

                            <population id="demographic.alltrees" spec="CompoundPopulationFunction" populationIndicators="@indicators.alltrees" populationSizes="@popSizes.alltrees">

                                <itree id="treeIntervals.t:alignment" spec="TreeIntervals" tree="@Tree.t:alignment"/>

                            </population>

                            <parameter id="RealParameter.7" lower="0.0" name="factor" upper="0.0">1.0</parameter>

                        </populationModel>

                        <treeIntervals idref="treeIntervals.t:alignment"/>

                    </distribution>

                    <prior id="indicatorsPrior.alltrees" name="distribution">

                        <x id="indsSun.alltrees" spec="util.Sum">

                            <arg idref="indicators.alltrees"/>

                        </x>

                        <distr id="Poisson.EBSP" spec="beast.math.distributions.Poisson">

                            <parameter id="RealParameter.8" estimate="false" name="lambda" upper="0.0">{lambdaPois}</parameter>

                        </distr>

                    </prior>

                    <prior id="KappaPrior.s:alignment" name="distribution" x="@kappa.s:alignment">

                        <LogNormal id="LogNormalDistributionModel.2" name="distr">

                            <parameter id="RealParameter.5" estimate="false" name="M">{moyLogNorm}</parameter>

                            <parameter id="RealParameter.6" estimate="false" name="S">{varLogNorm}</parameter>

                        </LogNormal>

                    </prior>

                    <prior id="popSizePrior.alltrees" name="distribution" x="@popSizes.alltrees">

                        <Exponential id="popPriorDist.EBSP" mean="@populationMean.alltrees" name="distr"/>

                    </prior>

                    <prior id="populationMeanPrior.alltrees" name="distribution" x="@populationMean.alltrees">

                        <OneOnX id="OneOnX.EBSP" name="distr"/>

                    </prior>

                </distribution>

                <distribution id="likelihood" spec="util.CompoundDistribution" useThreads="true">

                    <distribution id="treeLikelihood.alignment" spec="ThreadedTreeLikelihood" data="@alignment" tree="@Tree.t:alignment">

                        <siteModel id="SiteModel.s:alignment" spec="SiteModel">

                            <parameter id="mutationRate.s:alignment" estimate="false" name="mutationRate">{mutRate}</parameter>

                            <parameter id="gammaShape.s:alignment" estimate="false" name="shape">1.0</parameter>

                            <parameter id="proportionInvariant.s:alignment" estimate="false" lower="0.0" name="proportionInvariant" upper="1.0">0.0</parameter>

                            <substModel id="hky.s:alignment" spec="HKY" kappa="@kappa.s:alignment">

                                <frequencies id="empiricalFreqs.s:alignment" spec="Frequencies" data="@alignment"/>

                            </substModel>

                        </siteModel>

                        <branchRateModel id="StrictClock.c:alignment" spec="beast.evolution.branchratemodel.StrictClockModel">

                            <parameter id="clockRate.c:alignment" estimate="false" name="clock.rate">1.0</parameter>

                        </branchRateModel>

                    </distribution>

                </distribution>

            </distribution>

            <operator id="KappaScaler.s:alignment" spec="ScaleOperator" parameter="@kappa.s:alignment" scaleFactor="0.5" weight="{kappaWeight}"/>

            <operator id="ExtendedBayesianSkylineTreeScaler.t:alignment" spec="ScaleOperator" scaleFactor="0.5" tree="@Tree.t:alignment" weight="3.0"/>

            <operator id="ExtendedBayesianSkylineTreeRootScaler.t:alignment" spec="ScaleOperator" rootOnly="true" scaleFactor="0.5" tree="@Tree.t:alignment" weight="3.0"/>

            <operator id="ExtendedBayesianSkylineUniformOperator.t:alignment" spec="Uniform" tree="@Tree.t:alignment" weight="30.0"/>

            <operator id="ExtendedBayesianSkylineSubtreeSlide.t:alignment" spec="SubtreeSlide" tree="@Tree.t:alignment" weight="15.0" size="0.005"/>

            <operator id="ExtendedBayesianSkylineNarrow.t:alignment" spec="Exchange" tree="@Tree.t:alignment" weight="15.0"/>

            <operator id="ExtendedBayesianSkylineWide.t:alignment" spec="Exchange" isNarrow="false" tree="@Tree.t:alignment" weight="3.0"/>

            <operator id="ExtendedBayesianSkylineWilsonBalding.t:alignment" spec="WilsonBalding" tree="@Tree.t:alignment" weight="3.0"/>

            <operator id="bitflip.alltrees" spec="BitFlipOperator" parameter="@indicators.alltrees" weight="30.0"/>

            <operator id="indicatorSampler.alltrees" spec="SampleOffValues" dist="@popPriorDist.EBSP" indicators="@indicators.alltrees" values="@popSizes.alltrees" weight="{popWeight}"/>

            <operator id="indicatorScaler.alltrees" spec="ScaleOperator" degreesOfFreedom="1" indicator="@indicators.alltrees" parameter="@popSizes.alltrees" scaleFactor="0.5" weight="{popWeight}"/>

            <operator id="EBSPupDownOperator.alltrees" spec="UpDownOperator" scaleFactor="0.7" weight="5.0">

                <up idref="popSizes.alltrees"/>

                <up idref="populationMean.alltrees"/>

            </operator>

            <logger id="tracelog" fileName="{out}.log" logEvery="{logEvery}" model="@posterior" sanitiseHeaders="true" sort="smart">

                <log idref="posterior"/>

                <log idref="likelihood"/>

                <log idref="prior"/>

                <log idref="treeLikelihood.alignment"/>

                <log id="TreeHeight.t:alignment" spec="beast.evolution.tree.TreeHeightLogger" tree="@Tree.t:alignment"/>

                <log idref="kappa.s:alignment"/>

                <log idref="ExtendedBayesianSkyline.t:alignment"/>

                <log idref="indicators.alltrees"/>

                <log idref="populationMean.alltrees"/>

                <log idref="popSizes.alltrees"/>

                <log id="sumIndicators" spec="util.Sum">

                    <arg idref="indicators.alltrees"/>

                </log>

            </logger>

            <logger id="screenlog" logEvery="{logEvery}">

                <log idref="posterior"/>

                <log id="ESS.0" spec="util.ESS" arg="@posterior"/>

                <log idref="likelihood"/>

                <log idref="prior"/>

            </logger>

            <logger id="treelog.t:alignment" fileName="{out}.trees" logEvery="{logEvery}" mode="tree">

                <log id="TreeWithMetaDataLogger.t:alignment" spec="beast.evolution.tree.TreeWithMetaDataLogger" tree="@Tree.t:alignment"/>

            </logger>

            <logger id="EBSPLogger" fileName="{out}_EBSP.log" logEvery="{logEBSP}" model="@demographic.alltrees">

                <log idref="demographic.alltrees"/>

            </logger>

        </run>

        </beast>

        """.format(**dico)

    elif dico["type"] == "BSP":
        return """
        <map name="Uniform" >beast.math.distributions.Uniform</map>

        <map name="Exponential" >beast.math.distributions.Exponential</map>

        <map name="LogNormal" >beast.math.distributions.LogNormalDistributionModel</map>

        <map name="Normal" >beast.math.distributions.Normal</map>

        <map name="Beta" >beast.math.distributions.Beta</map>

        <map name="Gamma" >beast.math.distributions.Gamma</map>

        <map name="LaplaceDistribution" >beast.math.distributions.LaplaceDistribution</map>

        <map name="prior" >beast.math.distributions.Prior</map>

        <map name="InverseGamma" >beast.math.distributions.InverseGamma</map>

        <map name="OneOnX" >beast.math.distributions.OneOnX</map>

        <run id="mcmc" spec="MCMC" chainLength="{MCMC}">

            <state id="state" storeEvery="5000">

                <tree id="Tree.t:alignment" name="stateNode">

                    <taxonset id="TaxonSet.alignment" spec="TaxonSet">

                        <alignment idref="alignment"/>

                    </taxonset>

                </tree>

                <parameter id="kappa.s:alignment" lower="0.0" name="stateNode">1.0</parameter>

                <parameter id="bPopSizes.t:alignment" dimension="{groupSize}" lower="{minPop}" name="stateNode" upper="{maxPop}">{initPop}</parameter>

                <stateNode id="bGroupSizes.t:alignment" spec="parameter.IntegerParameter" dimension="{groupSize}">1</stateNode>

                <parameter id="freqParameter.s:alignment" dimension="4" lower="0.0" name="stateNode" upper="1.0">0.25</parameter>

            </state>

            <init id="RandomTree.t:alignment" spec="beast.evolution.tree.RandomTree" estimate="false" initial="@Tree.t:alignment" taxa="@alignment">

                <populationModel id="ConstantPopulation0.t:alignment" spec="ConstantPopulation">

                    <parameter id="randomPopSize.t:alignment" name="popSize">1.0</parameter>

                </populationModel>

            </init>

            <distribution id="posterior" spec="util.CompoundDistribution">

                <distribution id="prior" spec="util.CompoundDistribution">

                    <distribution id="BayesianSkyline.t:alignment" spec="BayesianSkyline" groupSizes="@bGroupSizes.t:alignment" popSizes="@bPopSizes.t:alignment">

                        <treeIntervals id="BSPTreeIntervals.t:alignment" spec="TreeIntervals" tree="@Tree.t:alignment"/>

                    </distribution>

                    <distribution id="MarkovChainedPopSizes.t:alignment" spec="beast.math.distributions.MarkovChainDistribution" jeffreys="true" parameter="@bPopSizes.t:alignment"/>

                    <prior id="KappaPrior.s:alignment" name="distribution" x="@kappa.s:alignment">

                        <LogNormal id="LogNormalDistributionModel.0" name="distr">

                            <parameter id="RealParameter.1" estimate="false" name="M">{moyLogNorm}</parameter>

                            <parameter id="RealParameter.2" estimate="false" name="S">{varLogNorm}</parameter>

                        </LogNormal>

                    </prior>

                </distribution>

                <distribution id="likelihood" spec="util.CompoundDistribution" useThreads="true">

                    <distribution id="treeLikelihood.alignment" spec="ThreadedTreeLikelihood" data="@alignment" tree="@Tree.t:alignment">

                        <siteModel id="SiteModel.s:alignment" spec="SiteModel">

                            <parameter id="mutationRate.s:alignment" estimate="false" name="mutationRate">{mutRate}</parameter>

                            <parameter id="gammaShape.s:alignment" estimate="false" name="shape">1.0</parameter>

                            <parameter id="proportionInvariant.s:alignment" estimate="false" lower="0.0" name="proportionInvariant" upper="1.0">0.0</parameter>

                            <substModel id="hky.s:alignment" spec="HKY" kappa="@kappa.s:alignment">

                                <frequencies id="estimatedFreqs.s:alignment" spec="Frequencies" frequencies="@freqParameter.s:alignment"/>

                            </substModel>

                        </siteModel>

                        <branchRateModel id="StrictClock.c:alignment" spec="beast.evolution.branchratemodel.StrictClockModel">

                            <parameter id="clockRate.c:alignment" estimate="false" name="clock.rate">1.0</parameter>

                        </branchRateModel>

                    </distribution>

                </distribution>

            </distribution>

            <operator id="KappaScaler.s:alignment" spec="ScaleOperator" parameter="@kappa.s:alignment" scaleFactor="0.5" weight="0.1"/>

            <operator id="BayesianSkylineTreeScaler.t:alignment" spec="ScaleOperator" scaleFactor="0.5" tree="@Tree.t:alignment" weight="3.0"/>

            <operator id="BayesianSkylineTreeRootScaler.t:alignment" spec="ScaleOperator" rootOnly="true" scaleFactor="0.5" tree="@Tree.t:alignment" weight="3.0"/>

            <operator id="BayesianSkylineUniformOperator.t:alignment" spec="Uniform" tree="@Tree.t:alignment" weight="30.0"/>

            <operator id="BayesianSkylineSubtreeSlide.t:alignment" spec="SubtreeSlide" tree="@Tree.t:alignment" weight="15.0"/>

            <operator id="BayesianSkylineNarrow.t:alignment" spec="Exchange" tree="@Tree.t:alignment" weight="15.0"/>

            <operator id="BayesianSkylineWide.t:alignment" spec="Exchange" isNarrow="false" tree="@Tree.t:alignment" weight="3.0"/>

            <operator id="BayesianSkylineWilsonBalding.t:alignment" spec="WilsonBalding" tree="@Tree.t:alignment" weight="3.0"/>

            <operator id="popSizesScaler.t:alignment" spec="ScaleOperator" parameter="@bPopSizes.t:alignment" scaleFactor="0.75" weight="{popWeight}"/>

            <operator id="groupSizesDelta.t:alignment" spec="DeltaExchangeOperator" integer="true" weight="6.0">

                <intparameter idref="bGroupSizes.t:alignment"/>

            </operator>

            <operator id="FrequenciesExchanger.s:alignment" spec="DeltaExchangeOperator" delta="0.01" weight="0.1">

                <parameter idref="freqParameter.s:alignment"/>

            </operator>

            <logger id="tracelog" fileName="{out}.log" logEvery="{logEvery}" model="@posterior" sanitiseHeaders="true" sort="smart">

                <log idref="posterior"/>

                <log idref="likelihood"/>

                <log idref="prior"/>

                <log idref="treeLikelihood.alignment"/>

                <log id="TreeHeight.t:alignment" spec="beast.evolution.tree.TreeHeightLogger" tree="@Tree.t:alignment"/>

                <log idref="kappa.s:alignment"/>

                <log idref="BayesianSkyline.t:alignment"/>

                <log idref="bPopSizes.t:alignment"/>

                <log idref="bGroupSizes.t:alignment"/>

                <log idref="freqParameter.s:alignment"/>

            </logger>

            <logger id="screenlog" logEvery="{logEvery}">

                <log idref="posterior"/>

                <log id="ESS.0" spec="util.ESS" arg="@posterior"/>

                <log idref="likelihood"/>

                <log idref="prior"/>

            </logger>

            <logger id="treelog.t:alignment" fileName="{out}.trees" logEvery="{logEvery}" mode="tree">

                <log id="TreeWithMetaDataLogger.t:alignment" spec="beast.evolution.tree.TreeWithMetaDataLogger" tree="@Tree.t:alignment"/>

            </logger>

            </run>

        </beast>

        """.format(**dico)
