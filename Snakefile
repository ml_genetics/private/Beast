import os
import subprocess

# Initialization of the snakemake parameters

#dir_path = "/home/jcury/work/Beast/Beast" # genotoul

dir_path = "/home/jean/Documents/Git/Beast/Beast" # local

BEAST = "beast"

indir = config["in"]
doplot = config["plot"]
conf = config["conf"]

try:
    infile = config["infile"]
except:
    pass
try:
    outdir=config["out"]
except:
    outdir="{}/output".format(indir)
    os.makedirs(outdir, exist_ok=True)
try:
    burnin=config["burnin"]
except:
    burnin = 0.1
try:
    ext=config["ext"]
except:
    ext=".npz"

# Copy the config file use for the analyse in the output directory
subprocess.call("cp {} {}/config".format(conf, outdir), shell=True)

# Create the list of the inputfile.
if config["read"] == "file":
    FILES = []
    # Save each filename in the current directory
    with open(config["infile"]) as file:
        for line in file:
            line = line.replace("./","").replace(".npz\n","")
            FILES.append(line)
elif config["read"] == "dir":
    FILES = [f.split(ext)[0] for f in os.listdir(config["in"]) if f.endswith(ext)]



rule all:
    input:
        expand("{dir}/beast_{file}.log", file=FILES, dir=outdir)

#Make xml file with the simulation
rule xml:
    input:
        indir + "/{file}"+ext
    output:
        outdir + "/{file}.xml"
    shell:
        dir_path+"/writeXML.py -in {input} -out {outdir} -xml {conf}"

#Run beast analysis for each simulation
rule beast:
    input:
        outdir + "/{file}.xml"
    output:
        outdir + "/beast_{file}.log"
    run:
        ess_min = 200
    	infile = wildcards.file
        while not os.path.isfile(outdir + "/beast_" + infile + ".log"):
            if ess_min <= 130:
                print("abort mission")
                shell("mv {outdir}/beast_{wildcards.file}_140.log {output}")
                break
            shell("{BEAST} {input} > {output}")
            shell("Rscript "+ dir_path +"/babette_ESS.R --vanilla {outdir} {wildcards.file} "+str(ess_min))
            ess_min-=10
        else:
            shell("Rscript "+ dir_path + "/plot.R --vanilla {outdir} {wildcards.file} {burnin} {doplot}")
