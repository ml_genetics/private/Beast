#!/usr/bin/env python
# -*- coding: utf-8 -*-
import pandas as pd
import numpy as np
import os
import warnings
warnings.simplefilter('ignore', FutureWarning)


def convert_ms(msfile, n_samples=None):
    """
    Read ms file, which contains:
        //
        segsites: 13
        positions: 0.01 0.2 ...
        00100
        00010
    write them on disk in compressed numpy format.
    """

    df = None
    positions = None
    pos_ok = False
    with open(msfile, "r") as file_in:
        sample = 0
        for i, line in enumerate(file_in):
            if "segsites:" in line.split():
                num_segsites = int(line.split()[1])
                if num_segsites == 0:
                    return
            elif "positions:" in line.split():
                positions = np.array([float(j) for j in line.split()[1:]])
                df = pd.DataFrame(columns=range(num_segsites), dtype=int)
                pos_ok = True
            elif pos_ok and line!="\n":
                df.loc[sample] = np.array([int(j) for j in line.split()[0]])
                sample += 1
                if sample == n_samples:
                    break
    if "msin" in msfile:
        outfile = os.path.splitext(msfile)[0] + "_ms"
        # remove columns without snp (because not present in the subsample)
        d = df.values
        # get columns where at least one element in the column is different from the first element
        positions = positions[(d!=d[0]).any(axis=0)]
        d = d[:, (d!=d[0]).any(axis=0)].astype(int)
    else:
        outfile = os.path.splitext(msfile)[0]
        d = df.values.astype(int)
    np.savez_compressed(outfile,
                        SNP=d,
                        POS=positions)


def read_ms_compressed(npzfile, key="all"):
    """
    Takes a .npz file and return all data (SNP and position).
    If one want to get only SNP matrix, set key="SNP",
    or key="POS" for only Position arrays.
    """
    data = np.load(npzfile)
    if key=="all":
        return data["SNP"], data["POS"]
    else:
        return data[key]


if __name__ == '__main__':
    pass
