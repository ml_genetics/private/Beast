# Beast analysis

Launch with the command-line :
`snakemake --config in=<input-directory> plot=<boolean> conf=<configxml.yaml> read=<file-or-dir> out={in}/output ext=".npz" burnin=0.1` in the directory Beast.
The program will make the beast analysis with the parameters given for each .msout files in the directory given.

- plot : this option takes the value "TRUE" or "FALSE". It allows or not the saving of the skyline plot in pdf.
- conf : it corresponds to the path to the config file containing the parameters for the xml file for the beast run.
- read : this option takes the value "file" or "dir". You choose if you want to read a file where the input names are written or if you want to run beast on all input files in the directory provided. If you choose "file" you have to provide the file containing the input names "file=<the-file>".
- out : the output directory, by default a directory called "output" will be created in the input directory.
- ext : an optional parameter to choose the extension of the input file (msout, npz or fasta). The default value is ".npz".
- burnin : an optional parameter to change the percentage of burnin in the sample (/100). The default value is 0.1.

## The snakefile

First, the snakefile reads config.yaml. Then, it creates a list depending on the number of iterations per file. It finds the file to analyze in the directory provided. The "rule all" indicates the expected files at the end. The "rule xml" takes in input the .msout or .npz or .fasta file and creates the xml file with the parameters given in the config.yaml. The "rule beast" runs beast on each xml files and write the shell output in a log file. It also renames the beast outputs adding the iteration id. The "rule plot" runs a Rscript which builds the skyline plot and saves it as a pdf file.

### Useful snakemake commands:

- `snakemake -np --reason`: Dry run with explanation
- `snakemake --gui` : open a browser with the DAG, and the possibility to start the pipeline from there.
- `snakemake --cores <number-of-cores>` : use the number of cores given to run the snakefile
- `snakemake --config <var_name> = <var_value>` : add a parameter to the config dictionary.

## writeXML.py and snp2xml.py

The script takes in input the file to analyze. Then it creates the xml file thanks to function you can find in "snp2xml.py". The header is just a comment so it's given by a simple function which return a string.
The sequence part is built by the function "ms_converter". For a file, if the npz file doesn't exist in the directory it's created. Then it's read : the SNP and the positions are extracted.
A random sequence is created with a length given in config.yaml. The same sequence is repeated for each individuals in the sample to create the matrix of alignment.
Then, the SNP positions are localized in the snp matrix provided by the npz file. A dictionary is created which contains in key the mutation for each mutated site. Each key contains a dictionary with the list of the 'x' and 'y' positions of the SNP in the matrix of alignment.
Once the dictionary is done, the matrix of alignment is changed according to the mutations obtained. Finally, the sequences are written in a string in the xml format and returned by the function.
The footer of the xml file is written in a string and returned by the function "footer". It takes in argument a dictionary which contains the parameters in config.yaml in order to place them in the xml footer.

## babette_ESS.R

This script selects the log file with acceptable ESS.
It uses the package "babette" which allows to parse the log file and calculate the ESS for each parameters. An ESS is acceptable with a value over 100 and good over 200. The script accepts three ESS between 100 and threshold fixed (200-130). At the fourth, Beast is reran and the threshold is reduced by 10. If the ESS are good enough, the results of the run pass to the next step.

## plot.R

This script save the data tp make the SP and the HPD of the activated points in Beast. A point corresponds to a change in the population size. The EBSP analysis in Beast activates some points to build the SP. If there is zero point activated it seems that the population is constant. At the end, there is one number of points by step in the Markov chain. The script build an confidence interval. If it contains zero the prediction is a constant population. If it not the prediction is a population in variation.
Plus, a boolean variable allows or not to save the plot in pdf format with the distribution of the number of activated points.

## Parameters in config.yaml

The configfile for the xml has to provide the parameters used for the analysis :
- type : the type of analysis wanted (BSP or EBSP)
- length : the sequence length,
- MCMC : the length of the Markov Chain,
- logEvery : beast will save the results each X iterations of Markov Chain for the log and trees output,
- logEBSP : same thing for the EBSP output,
- initPop : the initialization of the population size,
- maxPop : the maximum value for the population size,
- minPop : the minimum value for the population size,
- moyLogNorm : the mean parameter for the LogNormal distribution model which predict the kappa prior (transition/transversion ratio),
- varLogNorm : the variance parameter for the LogNormal
- lambdaPois : the lambda parameter for the poisson distribution model which predict the indicators priors.
- groupSize : the number of group of coalescent event.
- mutRate : the mutation rate.
- kappaWeight : the weight for the operator kappa which is the ratio of transition/transversion
- popWeight : the weight for the operator of the population size.

## Requirements

With command if you use conda.

- snakemake : `conda install -c bioconda snakemake`
- Beast v2:  [Github](https://github.com/CompEvol/beast2/releases/tag/v2.5.0)
- biopython: `conda install -c bioconda biopython`
- R: `conda install R`


## Examples

    snakemake --cores 3 --config in="Documents/simulations" plot="TRUE" conf="config.yaml" read="file"
    snakemake --config in="Documents/simulations" out="Document/output" plot="TRUE conf="config.yaml" read="file" burnin=0.2
